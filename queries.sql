SELECT * FROM uglabdb.people_uglab_class;

SELECT * FROM uglabdb.people_crn_list;

SELECT * FROM uglabdb.group_type;

SELECT * FROM uglabdb.crn_list;



SELECT t2.username,t3.crn FROM uglabdb.people_crn_list t1
LEFT OUTER JOIN uglabdb.people t2
ON t1.people_id=t2.id
LEFT OUTER JOIN uglabdb.crn_list t3
ON t1.crn_list_id =t3.id;

-- GIVES usename and class name
SELECT t2.username, t3.id as uglab_class_id, t3.name
	,t3.id AS class_id
-- ,t4.crn
FROM uglabdb.people_uglab_class t1
LEFT  JOIN uglabdb.people t2
ON t1.people_id=t2.id
LEFT  JOIN uglabdb.uglab_class t3
ON t1.uglab_class_id =t3.id

;



-- GIVES THE username AND crn number or NULL
SELECT t2.username, t3.crn,t3.uglab_class_id,t4.name FROM uglabdb.people_crn_list t1
RIGHT OUTER JOIN uglabdb.people t2
ON t1.people_id=t2.id
LEFT OUTER JOIN uglabdb.crn_list t3
ON t1.crn_list_id =t3.id

LEFT OUTER JOIN uglabdb.uglab_class t4
ON t3.uglab_class_id =t4.id

;

-- FINAL QUERY
SELECT q1.* ,q2.* FROM
	 (SELECT t2.username, t3.id as uglab_class_id,t3.name  FROM uglabdb.people_uglab_class t1
		LEFT  JOIN uglabdb.people t2
		ON t1.people_id=t2.id
		LEFT  JOIN uglabdb.uglab_class t3
		ON t1.uglab_class_id =t3.id) AS q1 

LEFT JOIN
 (SELECT t2.username,t3.crn,t3.uglab_class_id FROM uglabdb.people_crn_list t1
RIGHT OUTER JOIN uglabdb.people t2
ON t1.people_id=t2.id
LEFT OUTER JOIN uglabdb.crn_list t3
ON t1.crn_list_id =t3.id) AS q2
ON q1.username=q2.username
-- ON q1.uglab_class_id=q2.uglab_class_id
;

-- -- -- -- -- -- --
-- The two queries
-- -- -- -- -- -- -- 
SELECT t1.*, t2.name FROM crn_list t1
INNER JOIN uglab_class t2
on t1.uglab_class_id=t2.id
;

SELECT t2.username, t3.id as uglab_class_id, t3.name
	,t3.id AS class_id
FROM uglabdb.people_uglab_class t1
LEFT  JOIN uglabdb.people t2
ON t1.people_id=t2.id
LEFT  JOIN uglabdb.uglab_class t3
ON t1.uglab_class_id =t3.id
;

SELECT * FROM people_uglab_class;