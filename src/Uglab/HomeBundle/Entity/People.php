<?php

namespace Uglab\HomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * People
 */
class People
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $pictureUrl;

    /**
     * @var string
     */
    private $uin;

    /**
     * @var string
     */
    private $office;

    /**
     * @var string
     */
    private $officeHrs;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var \Uglab\HomeBundle\Entity\GroupType
     */
    private $groupType;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $uglabClasses;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $crns;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->uglabClasses = new \Doctrine\Common\Collections\ArrayCollection();
        $this->crns = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return People
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return People
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return People
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return People
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set pictureUrl
     *
     * @param string $pictureUrl
     * @return People
     */
    public function setPictureUrl($pictureUrl)
    {
        $this->pictureUrl = $pictureUrl;

        return $this;
    }

    /**
     * Get pictureUrl
     *
     * @return string 
     */
    public function getPictureUrl()
    {
        return $this->pictureUrl;
    }

    /**
     * Set uin
     *
     * @param string $uin
     * @return People
     */
    public function setUin($uin)
    {
        $this->uin = $uin;

        return $this;
    }

    /**
     * Get uin
     *
     * @return string 
     */
    public function getUin()
    {
        return $this->uin;
    }

    /**
     * Set office
     *
     * @param string $office
     * @return People
     */
    public function setOffice($office)
    {
        $this->office = $office;

        return $this;
    }

    /**
     * Get office
     *
     * @return string 
     */
    public function getOffice()
    {
        return $this->office;
    }

    /**
     * Set officeHrs
     *
     * @param string $officeHrs
     * @return People
     */
    public function setOfficeHrs($officeHrs)
    {
        $this->officeHrs = $officeHrs;

        return $this;
    }

    /**
     * Get officeHrs
     *
     * @return string 
     */
    public function getOfficeHrs()
    {
        return $this->officeHrs;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return People
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return People
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set groupType
     *
     * @param \Uglab\HomeBundle\Entity\GroupType $groupType
     * @return People
     */
    public function setGroupType(\Uglab\HomeBundle\Entity\GroupType $groupType = null)
    {
        $this->groupType = $groupType;

        return $this;
    }

    /**
     * Get groupType
     *
     * @return \Uglab\HomeBundle\Entity\GroupType 
     */
    public function getGroupType()
    {
        return $this->groupType;
    }

    /**
     * Add uglabClasses
     *
     * @param \Uglab\HomeBundle\Entity\UglabClass $uglabClasses
     * @return People
     */
    public function addUglabClass(\Uglab\HomeBundle\Entity\UglabClass $uglabClasses)
    {
        $this->uglabClasses[] = $uglabClasses;

        return $this;
    }

    /**
     * Remove uglabClasses
     *
     * @param \Uglab\HomeBundle\Entity\UglabClass $uglabClasses
     */
    public function removeUglabClass(\Uglab\HomeBundle\Entity\UglabClass $uglabClasses)
    {
        $this->uglabClasses->removeElement($uglabClasses);
    }

    /**
     * Get uglabClasses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUglabClasses()
    {
        return $this->uglabClasses;
    }

    /**
     * Add crns
     *
     * @param \Uglab\HomeBundle\Entity\CrnList $crns
     * @return People
     */
    public function addCrn(\Uglab\HomeBundle\Entity\CrnList $crns)
    {
        $this->crns[] = $crns;

        return $this;
    }

    /**
     * Remove crns
     *
     * @param \Uglab\HomeBundle\Entity\CrnList $crns
     */
    public function removeCrn(\Uglab\HomeBundle\Entity\CrnList $crns)
    {
        $this->crns->removeElement($crns);
    }

    /**
     * Get crns
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCrns()
    {
        return $this->crns;
    }
}
