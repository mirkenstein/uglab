<?php

namespace Uglab\HomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FileType
 */
class FileType
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $path;

    /**
     * @var integer
     */
    private $classNumber;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $files;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $assignments;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $categories;

    /**
     * @var \Uglab\HomeBundle\Entity\GroupType
     */
    private $groupType;

    /**
     * @var \Uglab\HomeBundle\Entity\UglabClass
     */
    private $uglabClass;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
        $this->assignments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return FileType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return FileType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return FileType
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set classNumber
     *
     * @param integer $classNumber
     * @return FileType
     */
    public function setClassNumber($classNumber)
    {
        $this->classNumber = $classNumber;

        return $this;
    }

    /**
     * Get classNumber
     *
     * @return integer 
     */
    public function getClassNumber()
    {
        return $this->classNumber;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return FileType
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return FileType
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add files
     *
     * @param \Uglab\HomeBundle\Entity\File $files
     * @return FileType
     */
    public function addFile(\Uglab\HomeBundle\Entity\File $files)
    {
        $this->files[] = $files;

        return $this;
    }

    /**
     * Remove files
     *
     * @param \Uglab\HomeBundle\Entity\File $files
     */
    public function removeFile(\Uglab\HomeBundle\Entity\File $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Add assignments
     *
     * @param \Uglab\HomeBundle\Entity\Assignment $assignments
     * @return FileType
     */
    public function addAssignment(\Uglab\HomeBundle\Entity\Assignment $assignments)
    {
        $this->assignments[] = $assignments;

        return $this;
    }

    /**
     * Remove assignments
     *
     * @param \Uglab\HomeBundle\Entity\Assignment $assignments
     */
    public function removeAssignment(\Uglab\HomeBundle\Entity\Assignment $assignments)
    {
        $this->assignments->removeElement($assignments);
    }

    /**
     * Get assignments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAssignments()
    {
        return $this->assignments;
    }

    /**
     * Add categories
     *
     * @param \Uglab\HomeBundle\Entity\FileCategory $categories
     * @return FileType
     */
    public function addCategory(\Uglab\HomeBundle\Entity\FileCategory $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \Uglab\HomeBundle\Entity\FileCategory $categories
     */
    public function removeCategory(\Uglab\HomeBundle\Entity\FileCategory $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set groupType
     *
     * @param \Uglab\HomeBundle\Entity\GroupType $groupType
     * @return FileType
     */
    public function setGroupType(\Uglab\HomeBundle\Entity\GroupType $groupType = null)
    {
        $this->groupType = $groupType;

        return $this;
    }

    /**
     * Get groupType
     *
     * @return \Uglab\HomeBundle\Entity\GroupType 
     */
    public function getGroupType()
    {
        return $this->groupType;
    }

    /**
     * Set uglabClass
     *
     * @param \Uglab\HomeBundle\Entity\UglabClass $uglabClass
     * @return FileType
     */
    public function setUglabClass(\Uglab\HomeBundle\Entity\UglabClass $uglabClass = null)
    {
        $this->uglabClass = $uglabClass;

        return $this;
    }

    /**
     * Get uglabClass
     *
     * @return \Uglab\HomeBundle\Entity\UglabClass 
     */
    public function getUglabClass()
    {
        return $this->uglabClass;
    }
}
