<?php

namespace Uglab\HomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CrnList
 */
class CrnList
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $crn;

    /**
     * @var string
     */
    private $weekday;

    /**
     * @var \DateTime
     */
    private $startTime;

    /**
     * @var \DateTime
     */
    private $endTime;

    /**
     * @var string
     */
    private $teachLoc;

    /**
     * @var string
     */
    private $username;

    /**
     * @var \Uglab\HomeBundle\Entity\GroupType
     */
    private $groupType;

    /**
     * @var \Uglab\HomeBundle\Entity\uglabClass
     */
    private $uglabClass;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $people;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->people = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set crn
     *
     * @param integer $crn
     * @return CrnList
     */
    public function setCrn($crn)
    {
        $this->crn = $crn;

        return $this;
    }

    /**
     * Get crn
     *
     * @return integer 
     */
    public function getCrn()
    {
        return $this->crn;
    }

    /**
     * Set weekday
     *
     * @param string $weekday
     * @return CrnList
     */
    public function setWeekday($weekday)
    {
        $this->weekday = $weekday;

        return $this;
    }

    /**
     * Get weekday
     *
     * @return string 
     */
    public function getWeekday()
    {
        return $this->weekday;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     * @return CrnList
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime 
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set endTime
     *
     * @param \DateTime $endTime
     * @return CrnList
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return \DateTime 
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set teachLoc
     *
     * @param string $teachLoc
     * @return CrnList
     */
    public function setTeachLoc($teachLoc)
    {
        $this->teachLoc = $teachLoc;

        return $this;
    }

    /**
     * Get teachLoc
     *
     * @return string 
     */
    public function getTeachLoc()
    {
        return $this->teachLoc;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return CrnList
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set groupType
     *
     * @param \Uglab\HomeBundle\Entity\GroupType $groupType
     * @return CrnList
     */
    public function setGroupType(\Uglab\HomeBundle\Entity\GroupType $groupType = null)
    {
        $this->groupType = $groupType;

        return $this;
    }

    /**
     * Get groupType
     *
     * @return \Uglab\HomeBundle\Entity\GroupType 
     */
    public function getGroupType()
    {
        return $this->groupType;
    }

    /**
     * Set uglabClass
     *
     * @param \Uglab\HomeBundle\Entity\uglabClass $uglabClass
     * @return CrnList
     */
    public function setUglabClass(\Uglab\HomeBundle\Entity\uglabClass $uglabClass = null)
    {
        $this->uglabClass = $uglabClass;

        return $this;
    }

    /**
     * Get uglabClass
     *
     * @return \Uglab\HomeBundle\Entity\uglabClass 
     */
    public function getUglabClass()
    {
        return $this->uglabClass;
    }

    /**
     * Add people
     *
     * @param \Uglab\HomeBundle\Entity\People $people
     * @return CrnList
     */
    public function addPerson(\Uglab\HomeBundle\Entity\People $people)
    {
        $this->people[] = $people;

        return $this;
    }

    /**
     * Remove people
     *
     * @param \Uglab\HomeBundle\Entity\People $people
     */
    public function removePerson(\Uglab\HomeBundle\Entity\People $people)
    {
        $this->people->removeElement($people);
    }

    /**
     * Get people
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPeople()
    {
        return $this->people;
    }
}
