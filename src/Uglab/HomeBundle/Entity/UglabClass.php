<?php

namespace Uglab\HomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UglabClass
 */
class UglabClass
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $number;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $fileTypes;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $crnList;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $peopleUglabClass;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fileTypes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->crnList = new \Doctrine\Common\Collections\ArrayCollection();
        $this->peopleUglabClass = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return UglabClass
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return UglabClass
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return UglabClass
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Add fileTypes
     *
     * @param \Uglab\HomeBundle\Entity\FileType $fileTypes
     * @return UglabClass
     */
    public function addFileType(\Uglab\HomeBundle\Entity\FileType $fileTypes)
    {
        $this->fileTypes[] = $fileTypes;

        return $this;
    }

    /**
     * Remove fileTypes
     *
     * @param \Uglab\HomeBundle\Entity\FileType $fileTypes
     */
    public function removeFileType(\Uglab\HomeBundle\Entity\FileType $fileTypes)
    {
        $this->fileTypes->removeElement($fileTypes);
    }

    /**
     * Get fileTypes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFileTypes()
    {
        return $this->fileTypes;
    }

    /**
     * Add crnList
     *
     * @param \Uglab\HomeBundle\Entity\CrnList $crnList
     * @return UglabClass
     */
    public function addCrnList(\Uglab\HomeBundle\Entity\CrnList $crnList)
    {
        $this->crnList[] = $crnList;

        return $this;
    }

    /**
     * Remove crnList
     *
     * @param \Uglab\HomeBundle\Entity\CrnList $crnList
     */
    public function removeCrnList(\Uglab\HomeBundle\Entity\CrnList $crnList)
    {
        $this->crnList->removeElement($crnList);
    }

    /**
     * Get crnList
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCrnList()
    {
        return $this->crnList;
    }

    /**
     * Add peopleUglabClass
     *
     * @param \Uglab\HomeBundle\Entity\People $peopleUglabClass
     * @return UglabClass
     */
    public function addPeopleUglabClass(\Uglab\HomeBundle\Entity\People $peopleUglabClass)
    {
        $this->peopleUglabClass[] = $peopleUglabClass;

        return $this;
    }

    /**
     * Remove peopleUglabClass
     *
     * @param \Uglab\HomeBundle\Entity\People $peopleUglabClass
     */
    public function removePeopleUglabClass(\Uglab\HomeBundle\Entity\People $peopleUglabClass)
    {
        $this->peopleUglabClass->removeElement($peopleUglabClass);
    }

    /**
     * Get peopleUglabClass
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPeopleUglabClass()
    {
        return $this->peopleUglabClass;
    }
}
