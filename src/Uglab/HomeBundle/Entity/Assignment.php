<?php

namespace Uglab\HomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Assignment
 */
class Assignment
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $hwDue;

    /**
     * @var boolean
     */
    private $isPublished;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $files;

    /**
     * @var \Uglab\HomeBundle\Entity\FileType
     */
    private $type;

    /**
     * @var \Uglab\HomeBundle\Entity\Syllabus
     */
    private $week;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Assignment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Assignment
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set hwDue
     *
     * @param integer $hwDue
     * @return Assignment
     */
    public function setHwDue($hwDue)
    {
        $this->hwDue = $hwDue;

        return $this;
    }

    /**
     * Get hwDue
     *
     * @return integer 
     */
    public function getHwDue()
    {
        return $this->hwDue;
    }

    /**
     * Set isPublished
     *
     * @param boolean $isPublished
     * @return Assignment
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished
     *
     * @return boolean 
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Assignment
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Assignment
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add files
     *
     * @param \Uglab\HomeBundle\Entity\File $files
     * @return Assignment
     */
    public function addFile(\Uglab\HomeBundle\Entity\File $files)
    {
        $this->files[] = $files;

        return $this;
    }

    /**
     * Remove files
     *
     * @param \Uglab\HomeBundle\Entity\File $files
     */
    public function removeFile(\Uglab\HomeBundle\Entity\File $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set type
     *
     * @param \Uglab\HomeBundle\Entity\FileType $type
     * @return Assignment
     */
    public function setType(\Uglab\HomeBundle\Entity\FileType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Uglab\HomeBundle\Entity\FileType 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set week
     *
     * @param \Uglab\HomeBundle\Entity\Syllabus $week
     * @return Assignment
     */
    public function setWeek(\Uglab\HomeBundle\Entity\Syllabus $week = null)
    {
        $this->week = $week;

        return $this;
    }

    /**
     * Get week
     *
     * @return \Uglab\HomeBundle\Entity\Syllabus 
     */
    public function getWeek()
    {
        return $this->week;
    }
}
