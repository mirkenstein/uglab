<?php

namespace Uglab\HomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GroupType
 */
class GroupType
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $shortName;

    /**
     * @var boolean
     */
    private $isStaff;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $people;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $crnList;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $fileTypes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->people = new \Doctrine\Common\Collections\ArrayCollection();
        $this->crnList = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fileTypes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return GroupType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return GroupType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set shortName
     *
     * @param string $shortName
     * @return GroupType
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * Get shortName
     *
     * @return string 
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Set isStaff
     *
     * @param boolean $isStaff
     * @return GroupType
     */
    public function setIsStaff($isStaff)
    {
        $this->isStaff = $isStaff;

        return $this;
    }

    /**
     * Get isStaff
     *
     * @return boolean 
     */
    public function getIsStaff()
    {
        return $this->isStaff;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return GroupType
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return GroupType
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add people
     *
     * @param \Uglab\HomeBundle\Entity\People $people
     * @return GroupType
     */
    public function addPerson(\Uglab\HomeBundle\Entity\People $people)
    {
        $this->people[] = $people;

        return $this;
    }

    /**
     * Remove people
     *
     * @param \Uglab\HomeBundle\Entity\People $people
     */
    public function removePerson(\Uglab\HomeBundle\Entity\People $people)
    {
        $this->people->removeElement($people);
    }

    /**
     * Get people
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPeople()
    {
        return $this->people;
    }

    /**
     * Add crnList
     *
     * @param \Uglab\HomeBundle\Entity\CrnList $crnList
     * @return GroupType
     */
    public function addCrnList(\Uglab\HomeBundle\Entity\CrnList $crnList)
    {
        $this->crnList[] = $crnList;

        return $this;
    }

    /**
     * Remove crnList
     *
     * @param \Uglab\HomeBundle\Entity\CrnList $crnList
     */
    public function removeCrnList(\Uglab\HomeBundle\Entity\CrnList $crnList)
    {
        $this->crnList->removeElement($crnList);
    }

    /**
     * Get crnList
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCrnList()
    {
        return $this->crnList;
    }

    /**
     * Add fileTypes
     *
     * @param \Uglab\HomeBundle\Entity\FileType $fileTypes
     * @return GroupType
     */
    public function addFileType(\Uglab\HomeBundle\Entity\FileType $fileTypes)
    {
        $this->fileTypes[] = $fileTypes;

        return $this;
    }

    /**
     * Remove fileTypes
     *
     * @param \Uglab\HomeBundle\Entity\FileType $fileTypes
     */
    public function removeFileType(\Uglab\HomeBundle\Entity\FileType $fileTypes)
    {
        $this->fileTypes->removeElement($fileTypes);
    }

    /**
     * Get fileTypes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFileTypes()
    {
        return $this->fileTypes;
    }
}
