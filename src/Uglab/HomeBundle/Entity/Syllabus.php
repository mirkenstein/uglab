<?php

namespace Uglab\HomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Syllabus
 */
class Syllabus
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $week;

    /**
     * @var \DateTime
     */
    private $startDate;

    /**
     * @var \DateTime
     */
    private $endDate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $assignments;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->assignments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set week
     *
     * @param string $week
     * @return Syllabus
     */
    public function setWeek($week)
    {
        $this->week = $week;

        return $this;
    }

    /**
     * Get week
     *
     * @return string 
     */
    public function getWeek()
    {
        return $this->week;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return Syllabus
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return Syllabus
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Add assignments
     *
     * @param \Uglab\HomeBundle\Entity\Assignment $assignments
     * @return Syllabus
     */
    public function addAssignment(\Uglab\HomeBundle\Entity\Assignment $assignments)
    {
        $this->assignments[] = $assignments;

        return $this;
    }

    /**
     * Remove assignments
     *
     * @param \Uglab\HomeBundle\Entity\Assignment $assignments
     */
    public function removeAssignment(\Uglab\HomeBundle\Entity\Assignment $assignments)
    {
        $this->assignments->removeElement($assignments);
    }

    /**
     * Get assignments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAssignments()
    {
        return $this->assignments;
    }
}
