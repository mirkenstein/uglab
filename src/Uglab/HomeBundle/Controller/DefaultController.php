<?php

namespace Uglab\HomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Doctrine\Common\Collections\Criteria;

class DefaultController extends Controller {

    /**
     * 
     * @Route("/")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $labClasses = $em->getRepository('HomeBundle:UglabClass')->findAll();
        $groups = $em->getRepository('HomeBundle:GroupType')->findByIsStaff(true);
        $weeks = $em->getRepository('HomeBundle:Syllabus')->findAll();
        return array('labClasses' => $labClasses, 'groups' => $groups, 'weeks'=>$weeks);
    }

    /**
     * @Route("/labs/{classNr}")
     * @Template()
     */
    public function labAction($classNr) {
        $em = $this->getDoctrine()->getManager();
        $uglabClass = $em->getRepository('HomeBundle:UglabClass')->findOneByNumber($classNr);
        $returnArray = array('classNr' => $classNr, 'fileType' => $uglabClass->getFileTypes());
        $criteria = Criteria::create()
//        ->where(Criteria::expr()->eq("birthday", "1982-02-17"))
                ->orderBy(
                array("weekday" => Criteria::DESC), array("startTime" => Criteria::DESC)
                )
//                ->setFirstResult(0)
//                ->setMaxResults(20)
        ;
        $crnList = $uglabClass->getCrnList()->matching($criteria);
        if (count($crnList) > 0)
            $returnArray['crnList'] = $crnList;
        else
            $returnArray['talist'] = $uglabClass->getPeopleUglabClass();

        return $returnArray;
    }

}
