var $container = $("#example1");
var $console = $("#example1console");
var $parent = $container.parent();
var autosaveNotification;
var cellProperties = {};
var dynamicColumns = [];
var user = [];


function negativeValueRenderer(instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    if (parseInt(value, 10) <= 0) { //if row contains negative number
        td.className = 'negative'; //add class "negative"
        td.style.color = 'red';
        td.style.background = '#EEE';
    }

    if (!value || value === '') {
        td.style.background = '#EEE';
    }

    if (value === 'Weight' || value === 'Total') {
        td.style.fontStyle = 'italic';
        td.style.background = 'cyan';
    }

}
Handsontable.renderers.registerRenderer('negativeValueRenderer', negativeValueRenderer);


activeTab = $(".nav-tabs li.active a").text();
var defaultColumns = [{
        data: "user.last_name",
        title: "Last Name "
    }, {
        data: "user.first_name",
        title: "First Name"
    }, {
        data: "user.username",
        title: "netid"
    }, {
        data: "user.UIN",
        type: 'numeric',
        title: "UIN"
    }, {
        data: "user.CRN",
        type: 'numeric',
        title: "CRN"
    }
    , {
        data: "user.final_grade",
        title: "Grade"
    }
]
        ;
var defaultSettings = {
    startRows: 8,
    startCols: 6,
    rowHeaders: true,
    minSpareRows: 1,
    columns: dynamicColumns,
    contextMenu: true
//   , columnSorting: {
//        column: 0, //CRN column
//        sortOrder: true 
//    }
};
dynamicColumns = defaultColumns;


$container.handsontable({
    data: [],
    dataSchema: {
        user: {username: null, first_name: null, last_name: null, UIN: null, CRN: null, final_grade: null},
//        Exams: {CRN: null, scores: {exam_1: null, exam_2: null, exam_3: null}},
        Labs: {CRN: null, scores: {lab_1: null, lab_2: null, lab_3: null, lab_4: null, lab_5: null, lab_6: null, lab_7: null, lab_8: null, lab_9: null}},
        Disc: {CRN: null, scores: {quiz_1: null, quiz_2: null, quiz_3: null, quiz_4: null, quiz_5: null}}
//        Grades: {CRN: null, scores: {Fin_1: null}},
    },
    startRows: 8,
    startCols: 6,
    rowHeaders: true,
    minSpareRows: 1,
    columns: dynamicColumns,
    contextMenu: true,
    columnSorting: true
//    , columnSorting: {
//        column: 0 //CRN column
//        , sortOrder: true //true for ascending
//    }
    ,
    afterChange: function (change, source) {
        var table = $(".nav-tabs li.active").text();
        if (source === 'loadData') {
            return; //don't save this change
        }
//        var index=(handsontable.sortingEnabled ? handsontable.sortIndex[change[0][0]][0] : 0);
//        var index = handsontable.sortIndex[change[0][0]][0];
        //        if ($parent.find('input[name=autosave]').is(':checked')) {
        clearTimeout(autosaveNotification);
        $.ajax({
            url: "mongosave",
            dataType: "json",
            type: "POST",
            data: JSON.stringify({
                saveall: false,
                table: table,
//                    val: handsontable.getDataAtRow(index)
                val: handsontable.getData()
            }),
            contentType: "application/json",
            //data: handsontable.getDataAtRow(change[0][0])
            complete: function (data) {
                $console.text('Autosaved (' + change.length + ' ' +
                        'cell' + (change.length > 1 ? 's' : '') + ')');
                autosaveNotification = setTimeout(function () {
                    $console.text('Changes will be autosaved');
                }, 1000);
            }
        });
//        }

    }
    ,
    beforeRemoveRow: function (index, amount) {
        var toRemove = [];
        var tableCurSorted = isSorted(handsontable);
        for (i = index; i < index + amount; i++) {
//            var idx = (handsontable.sortingEnabled ? handsontable.sortIndex[i][0] : 1);
            var idx = tableCurSorted ? handsontable.sortIndex[i][0] : i;
            toRemove.push(handsontable.getDataAtRow(idx).user.username);
        }
//        console.log(toRemove);
        $.ajax({
            url: "mongoremove",
            dataType: "json",
            data: {userlist: toRemove},
            type: "POST",
        });

    },
    cells: function (row, col, prop) {
        var cellProperties = {};
        cellProperties.renderer = "negativeValueRenderer";
        return cellProperties;
    }

});
var handsontable = $container.data('handsontable');

//Roster tab
$parent.find('a[name=roster]').click(function () {

    if (dynamicColumns.length > 5) {
        dynamicColumns = dynamicColumns.slice(0, 4);

    }
    addCRNcol("user");
    addGradecol();
    $container.handsontable("updateSettings", {columns: dynamicColumns, readOnly: false});
    handsontable.render();

    loadAjaxData($(this).text());
});

function isSorted() {
    return handsontable.sortingEnabled && typeof handsontable.sortColumn !== 'undefined';
}

//Exams tab
//addTab("Exams", "exam", 3);


//Discussion tab
addTab("Disc", "quiz", 5);

//LABS tab
addTab("Labs", "lab", 9);

addTab("Homework", "hw", 9);
/*
 * 
 * @param {type} table
 * @returns {undefined}
 */
function addTab(table, label, ncols) {
    return  $parent.find('a[name=load-' + table + '-score]').click(function () {
//firts four columns 
        dynamicColumns = dynamicColumns.slice(0, 4);
//the CRN col
        addCRNcol(table)
//extra columns    
        addExtraCols(table, label, ncols);
        $container.handsontable("updateSettings", {columns: dynamicColumns, readOnly: false});
        handsontable.render();
        loadAjaxData($(this).text());
    });

}


/*
 * Adds the CRM column from the respective subcollection
 */
function addCRNcol(table) {
    crnObj = new Object();
    crnObj.data = table + ".CRN";
    crnObj.title = "CRN";
    crnObj.type = "numeric";
    dynamicColumns.push(crnObj);
}

function addGradecol() {
    crnObj = new Object();
    crnObj.data = "user.final_grade";
    crnObj.title = "Grade";
    dynamicColumns.push(crnObj);
}
;

/*
 * Adds the scores columns. 
 * table is the string that matches the mongo collection. Also must be the same as the tab namnes.
 * label used for column headers and score key values
 */
function addExtraCols(table, label, ncols) {
    for (var i = 1; i <= ncols; i++) {
        col = new Object();
        col.data = table + ".scores." + label + "_" + i.toString();
        col.title = label.toUpperCase() + i.toString();
        col.type = "numeric";
        dynamicColumns.push(col);
    }
}

function loadAjaxData(table) {
    $.ajax({
        url: "mongo",
        dataType: 'json',
        type: 'POST',
        data: {table: table, crns: getActiveList()},
        success: function (res) {
            $console.text(table + ' Data loaded');
            handsontable.loadData(res.data);
        }
    });
    return false;
}

//Get the list of  CRNs from the button bar on the top
function getActiveList() {
    if (!$("input[name=showall]:checked").length) {
        return false;
    }
    var list = [];
    $("a.btn.active").each(function (i, v) {
        list.push(parseInt($(this).text()));
    })
    if (list.length > 0) {
        return list;
    } else {
        return [];
    }
}
$(".filterbar a").click(function () {
    $(this).toggleClass("active");
    var table = $("li.active").text();
    loadAjaxData(table);
//    console.log(getActiveList())
});


$parent.find('input[name=showall]').click(function () {
    if ($(this).is(':checked')) {
        $console.text('Filtering By CRN is ON');
        $(".filterbar").show();
    }
    else {
        $console.text('Filtering By CRN is OFF');
        $(".filterbar").hide();


    }
    var table = $("li.active").text();
    loadAjaxData(table);
});

//////////////////
//NOT IN USE
/////////////////////
$parent.find('button[name=authorize]').click(function () {
    var table = $(".nav-tabs li.active a").text();
    $.ajax({
        url: "mongoauthorize",
        type: 'POST',
        success: function (res) {

            if (res.result === 'ok') {
                $console.text('Users from the class roster authorized');
            }
            else {
                $console.text('Save error');
            }
        },
        error: function () {
            $console.text('Save error!!!');
        }
    });
});

$parent.find('button[name=gethwscores]').click(function () {
    var table = $(".nav-tabs li.active a").text();
    $.ajax({
        url: "mongomerge",
        type: 'POST',
        success: function (res) {

            if (res.result === 'ok') {
                $console.text('HW scores posted in roster');
            }
            else {
                $console.text('Server error');
            }
        },
        error: function () {
            $console.text('Server error!!!');
        }
    });
});

$parent.find('input[name=autosave]').click(function () {
    if ($(this).is(':checked')) {
        $console.text('Changes will be autosaved');
    }
    else {
        $console.text('Changes will not be autosaved');
    }
});


