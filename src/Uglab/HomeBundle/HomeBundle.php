<?php

namespace Uglab\HomeBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class HomeBundle extends Bundle {

    public function getParent() {
        return 'MnmGradingBundle';
    }

}
