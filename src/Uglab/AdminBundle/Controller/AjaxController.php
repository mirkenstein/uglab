<?php

namespace Uglab\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;

class AjaxController extends Controller {

    /**
     * @Route("/getlist")
     * @Template()
     */
    public function getlistAction() {
        $em = $this->getDoctrine()->getManager();
        
        $response = new JsonResponse();
        $response->setData(array(1, 2, 3));
        return $response;
    }

    /**
     * @Route("/updatelist")
     * @Template()
     */
    public function updatelistAction() {
        return array(
                // ...
        );
    }

}
