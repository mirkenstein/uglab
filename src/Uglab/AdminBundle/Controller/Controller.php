<?php

namespace Uglab\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;

/**
 * Description of Controller
 *
 * @author mnm
 */
class Controller extends BaseController {

    public function joinTables() {
        $em = $this->getDoctrine()->getManager();

        $sqlQueryCrn = 'SELECT cl.crn , cl.start_time,cl.end_time, cl.weekday,
                    p.username ,ugc.number FROM crn_list cl
                    INNER JOIN uglab_class ugc
                    ON cl.uglab_class_id=ugc.id
                    LEFT JOIN people_crn_list pcl
                    ON pcl.crn_list_id=cl.id
                    LEFT JOIN people p
                    ON p.id=pcl.people_id'

        ;


        $stmt = $em->getConnection()->prepare($sqlQueryCrn);
        $stmt->execute();
        $crnPiece = $stmt->fetchAll();

        $sqlQueryPeople = 'SELECT
                    p.username,
                    p.first_name, p.last_name,
                    ugc.number FROM people p
                    RIGHT JOIN people_uglab_class pugc
                    ON p.id=pugc.people_id
                    RIGHT JOIN uglab_class ugc
                    ON ugc.id=pugc.uglab_class_id'
        ;
        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sqlQueryPeople);
        $stmt->execute();
        $peoplePiece = $stmt->fetchAll();

        $result = array();
        $merged = array();
        $people = array(); //Stores the people without crn
        foreach ($crnPiece as $crn) {
            foreach ($peoplePiece as $person) {
                if ($crn['number'] == $person['number'] && $crn['username'] == $person['username']) {
                    $merged = array_merge($crn, $person);
                    array_push($result, $merged);
                } 
            }
            if (!empty($merged)) {
                $merged = array();
            } else {
                array_push($result, $crn);
            }
        }
//        return $people;
        return $result;
    }

}
