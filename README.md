Clone a new copy
========================
get composer if you don't have it -

 `curl -s https://getcomposer.org/installer | php`
 
 then install all the shit
 
 `./composer.phar install`

If you don't have mongodb up and running already,  you will need to install mongodb from synaptic (on Ubuntu),  then run

 `sudo pecl install mongo`

After this follow the instrucitons and add the extension to the 3 php.ini files (in /etc/php5/...)

If these guys aren't added, you will need all 3

 `git submodule add  git@bitbucket.org:mirkenstein/mnmgradingbundle.git ./src/Mnm/MnmGradingBundle`

 `git submodule add  git@bitbucket.org:mirkenstein/mnmuserbundle.git ./src/Mnm/MnmUserBundle`

 `git submodule add  git@bitbucket.org:mirkenstein/mnmbluestembundle.git ./src/Mnm/MnmBluestemBundle`


Doctrine Database
========================
Yml files in `Resoruces\Config\Doctrine`
Once you create the yml entities files run first. 

 `php app/console doctrine:generate:entities HomeBundle --no-backup`

Need to run generate entities every time the schema is changed.
Create the database  and schema

 `php app/console doctrine:database:create`

 `php app/console doctrine:schema:create`

Drop schema or update 
`php app/console doctrine:schema:drop --force`

 `php app/console doctrine:schema:update --force`

 `./app/console khepin:yamlfixtures:load`
 
 
 
1) Clone the repository
----------------------------------

     git clone git@bitbucket.org:mirkenstein/phys105.git

     git submodule update --init

     ./permissions.sh

2) Create database,schema and insert fixtures
----------------------------------------------
     php app/console doctrine:database:create
     php app/console doctrine:schema:create
     php app/console khepin:yamlfixtures:load

Other commands

     php app/console doctrine:generate:entities HomeBundle --no-backup
     php app/console doctrine:schema:drop --force
     php app/console khepin:yamlfixtures:load --purge-orm 